# include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* mesDeAnio(int mes){
	char *nombre_mes;
	switch(mes){
		case 0:
			nombre_mes = "Enero";
			break;
		case 1:
			nombre_mes = "Febrero";
			break;
		case 2:
			nombre_mes = "Marzo";
			break;
		case 3:
			nombre_mes = "Abril";
			break;
		case 4:
			nombre_mes = "Mayo";
			break;
		case 5:
			nombre_mes = "Junio";
			break;
		case 6:
			nombre_mes = "Julio";
			break;						
		case 7:
			nombre_mes = "Agosto";
			break;
		case 8:
			nombre_mes = "Septiembre";
			break;

		case 9:
			nombre_mes = "Octubre";
			break;
		case 10:
			nombre_mes = "Noviembre";
			break;				
		case 11:
			nombre_mes = "Diciembre";
			break;

		default:
			nombre_mes = "Este número no representa ningún mes dentro  del calendario oficial";
			break;
	
	}
	return nombre_mes;
			
}

int main(int argc, char const *argv[])
{	
	int mes = -1;
	scanf("%d",&mes);
	printf("%s\n",mesDeAnio(mes));
	return 0;
}