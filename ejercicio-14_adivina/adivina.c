#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define CHANCES 5

int main(int argc, char const *argv[])
{
	int number_guess = (rand() % 100 ) + 1; //Se le agrego el 1 para corraborar que se encuentre el número entre [1,100]
	int number_choose;
	char *leyend = "PERDISTE";

	for (int i = 0; i < CHANCES; i++)
	{
		printf("%s","Ingresa un número: ");
		scanf("%d",&number_choose);
		if (number_choose == number_guess)
		{
			leyend = "GANASTE";
			break;
		}
	}
	printf("%s\n",leyend);
	printf("%s : %d \n","El número era", number_guess);
	return 0;
}