# include <stdio.h>
#include <stdlib.h>

void tablas(int multiplicador){
	for (int i = 2; i <= 10; i++)
	{
		printf("%d x %d = %d\n",i,multiplicador, (i*multiplicador));
	}
}

int main()
{
	int multiplicador = 0;
	printf("%s\n","Ingrese multiplicador: ");
	scanf("%d",&multiplicador);
	tablas(multiplicador);
	return 0;
}