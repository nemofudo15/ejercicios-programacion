#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h> 
#include <string.h>

char* opcion_coin(int opcion){
	if (opcion == 0)
	{
		return "AGUILA";
	}else{
		return "SOL";
	}
}


double gamble_to_make(){
	srand(time(0));
	double gamble = (rand() % 81) + 20;
	return gamble;
}

int main()
{
	char *ganador_cpu = "-------------------------------------Gano la computadora------------------------------------";
	char *ganador_user = "-------------------------------------Gano el usuario---------------------------------------"; 
	double user_money = 500;
	double cpu_money = 500;
	int winnings_user = 0;
	int winnings_cpu = 0;
	char user_choice[6];
	char * cpu_choice;
	for (int i = 0; i < 3; i++)
	{
		srand(time(0)); /*Lo utilizo para generar un número aleatorio distinto */
		char *coin_face =  (rand() %2) == 0 ? "AGUILA": "SOL" ;
		double gambling_user, gambling_cpu;
		/*Dinero de la apuesta*/
		printf("%s\n","Cuanto dinero vas a apostar recuerda entre $20.00 y $100.00");
		scanf("%lf", &gambling_user);
		gambling_cpu = gamble_to_make();
		printf("La computadora decidio apostar: $%lf \n", gambling_cpu);

		/*Elección de cara de moneda*/
		printf("%s\n","Qué eliges AGUILA o SOL");
		scanf("%s",user_choice);
		srand(time(0)); 

		cpu_choice = opcion_coin(rand() %2);
		printf("La computadora eligio: %s \n",cpu_choice);
		if (strcmp(user_choice ,coin_face) || strcmp(cpu_choice , coin_face))
		{
			if (strcmp(user_choice ,coin_face) &&  !(strcmp(cpu_choice , coin_face)))
			{
				winnings_user+=1;
				user_money+= gambling_cpu;
				cpu_money-= gambling_cpu;

			}else if (! (strcmp(user_choice ,coin_face)) &&  strcmp(cpu_choice , coin_face))
			{
				winnings_cpu+=1;
				cpu_money += gambling_user;
				user_money -= gambling_user; 
			}else{
				
				winnings_user+=1;
				winnings_cpu+=1;
				user_money += gambling_cpu;
				user_money -= gambling_user;
				cpu_money -= gambling_cpu;
				cpu_money += gambling_user;
			}
			
		}

		printf("La cara que salio en está ronda es: %s \n",coin_face);


		
	}
	if (winnings_cpu > winnings_user)
	{
		printf("%s\n",ganador_cpu);
	}else if (winnings_cpu < winnings_user) 
	{
		printf("%s\n",ganador_user );
	}else{
		if (cpu_money > user_money)
		{
			printf("%s\n",ganador_cpu);
		} else if (cpu_money < user_money)
		{
			printf("%s\n",ganador_user);
		}else{
			printf("%s\n","------EMPATE------- " );
		}
	}

	printf("TOTAL DE USUARIO: %lf \n", user_money);

	printf("TOTAL DE COMPUTADORA: %lf \n", cpu_money);

	return 0;
}