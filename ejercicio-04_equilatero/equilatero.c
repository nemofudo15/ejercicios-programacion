# include <stdio.h>
#include <stdlib.h>

double perimEquilatero(double lado_a, double lado_b,double lado_c)
{
	double perimetro = 0;
	if (!((lado_a == lado_b ) && (lado_b == lado_c)))
	{
		printf("%s\n","Error: Alguno de los lados no es igual a los demás" );
	}else if ((lado_a < 0)){
		printf("%s\n","Error: Los lados de los triangulos no deben tener valores negativos");
	}else{
		perimetro = 3 * lado_a;
	}
	return perimetro;
}

int main()
{
	double *lados; 
	lados = (double*) calloc(3,  sizeof (*lados));

	for (int i = 0; i < 3; i++)
	{
		scanf("%lf",&lados[i]);
	}  
	double perimetro = perimEquilatero(lados[0],lados[1],lados[2]);

	printf("%lf\n",perimetro);
	free(lados);
	return 0;
}