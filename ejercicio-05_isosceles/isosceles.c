# include <stdio.h>
#include <stdlib.h>

double perimIsoceles(double lado_a, double lado_b,double lado_c){
	double  perimetro = 0;
	if (! ( ((lado_a == lado_b) && (lado_b != lado_c)) || ((lado_b == lado_c) && (lado_c != lado_a)) || ((lado_a == lado_c) && (lado_c != lado_b)) ))
	{	
		printf("%s\n","ERROR: Todos  los lados son distintos o todos son iguales ");
	
	}else if(lado_a < 0 || lado_b <0 || lado_c < 0){
		
		printf("%s\n","ERROR: Todos los lados deben de tener valor positivo");	 

	}	
	else 
	{
		perimetro = lado_a + lado_b + lado_c; 	
	}
	
	return perimetro;	
}

int main()
{
	double *lados; 
	lados = (double*) calloc(3,  sizeof (*lados));

	for (int i = 0; i < 3; i++)
	{
		scanf("%lf",&lados[i]);
	}  
	double perimetro = perimIsoceles(lados[0],lados[1],lados[2]);

	printf("%lf\n",perimetro);
	free(lados);
	return 0;
}
