# include <stdio.h>
#include <stdlib.h>

/**
*ACLARACIÓN AL IGUAL QUE EL EJERCICIO 9 el lenguaje c no 
*tiene predefinido el tipo boolean por lo que se suplanta
* con integers (1/0) 
*/
int  anioBisiesto(int anio)
{
	if (anio % 4 == 0)
	{
		return 1;
	}
	else
	{
	 
		return 0;
	}
}		


int main()
	{
		int anio;
		int esbisiesto;
		printf("%s","Ingresa año: " );
		scanf("%d", &anio);
		esbisiesto = anioBisiesto(anio);
		if (esbisiesto)
		{
			printf("%s\n","True" );
		}else
		{
			printf("%s\n","False" );
		}
		return 0;
	}			