# include <stdio.h>
#include <stdlib.h>

double  perimEscaleno(double lado_a, double lado_b,double lado_c){
	double perimetro = 0;
	if  (! ((lado_a == lado_b)  && (lado_b ==  lado_c)))
	{
		if (! ((lado_a == lado_b) || ( lado_a == lado_c) || ( lado_c == lado_b)))
		{
			if (lado_a < 0 || lado_b < 0 || lado_c < 0)
			{
				printf("%s\n","Error: Los lados de los triangulos no deben tener valores negativos");
			}
			else{ 	
			 perimetro = lado_a + lado_b +lado_c;
			} 
		}	
		else
		{
			printf("%s\n"," Error hay dos lados del mismo tamaño por lo que no se puede calcular el perimetro");
		}
			
	}	
	else
	{
		printf("%s\n","Error todos los lados son iguales");	
	}
		
	return perimetro;	
	
}

int main()
{
	double *lados; 
	lados = (double*) calloc(3,  sizeof (*lados));

	for (int i = 0; i < 3; i++)
	{
		scanf("%lf",&lados[i]);
	}  
	double perimetro = perimEscaleno(lados[0],lados[1],lados[2]);

	printf("%lf\n",perimetro);
	free(lados);
	return 0;
	return 0;
}