boolean fechaReal(int dia, int mes, int anio):

	boolean validez = False
	Si(dia > 0 AND (mes > 0 AND mes < 13) ):
		SI (mesesTreinta(mes) AND dia <= 30):
			validez = True
		
		SINO(mesesTreintauno(mes) AND dia <= 31):
			validez = True
		
		SINO ((anioBisiesto(anio) AND dia <= 29) OR( (NOT anioBisiesto(anio)) AND dia <= 28)):
			validez = True	
		
		FINSI
		
	FINSI
	REGRESA validez; 




/*mesesTreinta y mesesTreintaUno son funciones que como su nombre me indica me verificaran si al mes le corresponde esa cantidad de días.
No se puso explicitamente cuales meses les corresponde cada cantidad con el fín de modular el programa y sea más sencillo de entender para los otros */
