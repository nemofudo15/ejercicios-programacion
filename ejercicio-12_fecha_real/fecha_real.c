# include <stdio.h>
#include <stdlib.h>



int mesestreinta(int mes){
	if (mes == 4 || mes ==6  || mes == 9 || mes ==11)
	{
		return 1;
	}else{
		return 0;
	}
}


int mesestreintauno(int mes){
	if (mes == 1 || mes == 3  || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
	{
		return 1;
	}else{
		return 0;
	}
}

int  anioBisiesto(int anio)
{
	if (anio % 4 == 0)
	{
		return 1;
	}
	else
	{
	 
		return 0;
	}
}	


int fechaReal(int dia, int mes, int anio)
{
	int validez = 0;
	if(dia > 0 && (mes > 0 && mes <= 12) ){ 
		if (mesestreinta(mes) && dia <= 30)
		{
			validez = 1;
		}
		else if(mesestreintauno(mes) && dia <= 31)
		{
			validez = 1;
		}
		else if ((anioBisiesto(anio) && dia <= 29) ||( (! anioBisiesto(anio)) && dia <= 28) )
		{
			validez = 1;	
		}
		
	}
	return validez; 
}

int main()
{
	int  *fecha; 
	int es_real;
	fecha = (int*) calloc(3,  sizeof (*fecha));
	for (int i = 0; i < 3; i++)
	{
		if (i == 0 )
		{
			printf("%s", "Ingresa dia: ");
		}
		
		if (i == 1 )
		{
			printf("%s", "Ingresa número de mes: ");
		}
		
		if (i == 2 )
		{
			printf("%s", "Ingresa año: ");
		}
		scanf("%d",&fecha[i]);
	} 
	es_real = fechaReal(fecha[0],fecha[1],fecha[2]);
	if(es_real){
		printf("%s\n","True");
	}
	else
	{
		printf("%s\n","False");
	}
	free(fecha); 
	return 0;
}