# include <stdio.h>
#include <stdlib.h>


double  perimEscaleno(double lado_a, double lado_b,double lado_c){
	double perimetro = 0;
	if  (! ((lado_a == lado_b)  && (lado_b ==  lado_c)))
	{
		if (! ((lado_a == lado_b) || ( lado_a == lado_c) || ( lado_c == lado_b)))
		{
			if (lado_a < 0 || lado_b < 0 || lado_c < 0)
			{
				printf("%s\n","Error: Los lados de los triangulos no deben tener valores negativos");
			}
			else{ 	
			 perimetro = lado_a + lado_b +lado_c;
			} 
		}	
		else
		{
			printf("%s\n"," Error hay dos lados del mismo tamaño por lo que no se puede calcular el perimetro");
		}
			
	}	
	else
	{
		printf("%s\n","Error todos los lados son iguales");	
	}
		
	return perimetro;	
	
}


double perimIsoceles(double lado_a, double lado_b,double lado_c){
	double  perimetro = 0;
	if (! ( ((lado_a == lado_b) && (lado_b != lado_c)) || ((lado_b == lado_c) && (lado_c != lado_a)) || ((lado_a == lado_c) && (lado_c != lado_b)) ))
	{	
		printf("%s\n","ERROR: Todos  los lados son distintos o todos son iguales ");
	
	}else if(lado_a < 0 || lado_b <0 || lado_c < 0){
		
		printf("%s\n","ERROR: Todos los lados deben de tener valor positivo");	 

	}	
	else 
	{
		perimetro = lado_a + lado_b + lado_c; 	
	}
	
	return perimetro;	
}

double perimEquilatero(double lado_a, double lado_b,double lado_c)
{
	double perimetro = 0;
	if (!((lado_a == lado_b ) && (lado_b == lado_c)))
	{
		printf("%s\n","Error: Alguno de los lados no es igual a los demás" );
	}else if ((lado_a < 0)){
		printf("%s\n","Error: Los lados de los triangulos no deben tener valores negativos");
	}else{
		perimetro = 3 * lado_a;
	}
	return perimetro;
}

double perimetroTriangulo(){
	double perimetro = 0;
	int  opcion; 
	double *lados;

	lados = (double*) calloc(3,  sizeof (*lados));
	for (int i = 0; i < 3; i++)
	{
		printf("Inserte el lado %c ",97+i);
		scanf("%lf",&lados[i]);
		printf("\n");
	}  
	printf("Elige tipo de triangulo:\n");
	printf("	1) Equilatero \n");
	printf("	2) Isoceles \n");
	printf("	3) Escaleno \n");
	scanf("%d",&opcion); 


	switch(opcion){
		case 1:
			perimetro =	perimEquilatero(lados[0], lados[1], lados[2]);
			break;
		case 2:
			perimetro = perimIsoceles(lados[0], lados[1], lados[2]);
			break;
		case 3:
			perimetro = perimEscaleno(lados[0], lados[1], lados[2]);
			break;
		default:
			printf("%s\n", "Error: la opcion insertada no es ningún tipo de triangulo conocido");	
			break;
	}
	free(lados);
	return perimetro;
}


int main(int argc, char const *argv[])
{
	double perim = perimetroTriangulo();
	printf("%lf\n",perim);
	return 0;
}