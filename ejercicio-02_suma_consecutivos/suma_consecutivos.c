/*
*Notas: Por default me piden que devuelva la suma , no que imprima aunque desde
*mi punto de vista creo que la mejor opción para mi fue imprimir el resultado de
*la función para poder demostrar la correctez del algoritmo implementado

*/

# include <stdio.h>
int sumaConsecutivos(int cota_s){
	int acumulador = 0;
	if (cota_s < 1 || cota_s > 50)
	{
		printf("%s\n","Error el numero no se encuentra en el intervalo especificado");
	}else{
		for (int i = 1; i <= cota_s; i++){
			acumulador+=i;
		}
	}
	return acumulador;
}


int main()
{	
	int cota_superior = 0;
	scanf("%d",&cota_superior);

	printf("%d\n", sumaConsecutivos(cota_superior));
	return 0;
}