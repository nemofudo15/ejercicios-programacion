# include <stdio.h>
#include <stdlib.h>
/*
*Como c no tiene tiene implementado el tipo boolean 
*devolvera 1 si es positivo y 0 si es negativo 
*/
int desigualdadT(double lado_a, double lado_b, double lado_c){
	if ((lado_a < (lado_b +lado_c)) && (lado_b < (lado_a +lado_c)) && (lado_c < (lado_a +lado_b)))
	{
		return 1;
	}else{
		return 0;
	}
}


int main()
{
	double *lados; 
	int es_triangulo;
	lados = (double*) calloc(3,  sizeof (*lados));

	for (int i = 0; i < 3; i++)
	{
		scanf("%lf",&lados[i]);
	}  
	es_triangulo = desigualdadT(lados[0],lados[1],lados[2]);
	if (es_triangulo == 1)
	{
		printf("%s\n","True");
	}else{
		printf("%s\n","False");
	}
	
	return 0;
}